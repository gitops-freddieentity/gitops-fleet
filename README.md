```sh
export GITLAB_TOKEN=xxxxx
flux bootstrap gitlab --hostname=gitlab.com --owner=gitops-freddieentity --repository=gitops-fleet --branch=main --path=./clusters/dev --token-auth
flux create source git guestbook \
    --url https://gitlab.com/gitops-freddieentity/gitops-fleet \
    --branch main \
    --interval 30s \
    --export \
    | tee ./clusters/dev/guestbook.yaml

flux create kustomization guestbook-dev \
    --source guestbook \
    --path "./" \
    --prune true \
    --interval 10m \
    --export \
    | tee -a ./clusters/dev/guestbook-dev.yaml

flux create helmrelease \
    ingress-nginx \
    --source GitRepository/devops-toolkit \
    --values values.yaml \
    --chart "helm" \
    --target-namespace ingress-nginx \
    --interval 30s \
    --export \
    | tee ./clusters/dev/infra/ingress-nginx.yaml
```